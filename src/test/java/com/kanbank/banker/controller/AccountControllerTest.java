package com.kanbank.banker.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import com.kanbank.banker.dto.AccountDto;
import com.kanbank.banker.service.AccountService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.RequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.io.IOException;

import static org.mockito.Matchers.any;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.header;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(MockitoJUnitRunner.class)
public class AccountControllerTest {

	@Mock
	private AccountService accountService;

	@InjectMocks
	private AccountController accountController;

	private MockMvc mockMvc;

	@Before
	public void setup() throws IOException {
		MockitoAnnotations.initMocks(this);
		mockMvc = MockMvcBuilders.standaloneSetup(accountController).build();
	}

	@Test
	public void apiFoundTest() throws Exception{
		RequestBuilder requestBuilder = MockMvcRequestBuilders.get("/accounts");
		mockMvc.perform(requestBuilder)
				.andExpect(status().isNoContent());
	}

	@Test
	public void apiNotFoundTest() throws Exception{
		RequestBuilder requestBuilder = MockMvcRequestBuilders.get("/accounts-not-there");
		mockMvc.perform(requestBuilder)
				.andExpect(status().isNotFound());
	}

	@Test
	public void postValidAccountAndExpectCreated() throws Exception {
		// given
		AccountDto account = AccountDto.createFullyPopulated("Kan", 123456, 12345678);
		when(accountService.saveOrUpdateAccount(any(AccountDto.class))).thenReturn(account);
		RequestBuilder requestBuilder = MockMvcRequestBuilders.post("/accounts")
				.contentType(MediaType.APPLICATION_JSON)
				.content(convertAccountToString(account).getBytes());

		// when & then
		mockMvc.perform(requestBuilder)
				.andExpect(status().isCreated())
				.andExpect(header().string(HttpHeaders.LOCATION, "http://localhost/accounts/" + account.getId()));
	}

	private String convertAccountToString(AccountDto account) throws Exception {
		ObjectWriter ow = new ObjectMapper().writer().withDefaultPrettyPrinter();
		return ow.writeValueAsString(account);
	}
}
