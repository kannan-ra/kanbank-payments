package com.kanbank.banker.configuration;

import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

@Configuration
@EnableJpaRepositories(value = "com.kanbank.banker.repository")
@PropertySource(value = {"classpath:properties/app-test.properties"})
public class TestAppConfig extends WebMvcConfigurerAdapter {
}