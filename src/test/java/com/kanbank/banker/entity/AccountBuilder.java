package com.kanbank.banker.entity;

import org.springframework.test.util.ReflectionTestUtils;

public class AccountBuilder
{
	private Long id = -1L;
	private String fullName = "name";
	private int sortCode = 0;
	private int accountNum = 0;

	public Account build() {
		Account account = new Account(fullName, sortCode, accountNum);
		ReflectionTestUtils.setField(account, "id", id);
		return account;
	}

	public AccountBuilder setId(Long id) {
		this.id = id;
		return this;
	}

	public AccountBuilder setFullName(String fullName) {
		this.fullName = fullName;
		return this;
	}

	public AccountBuilder setSortCode(int sortCode) {
		this.sortCode = sortCode;
		return this;
	}

	public AccountBuilder setAccountNum(int accountNum) {
		this.accountNum = accountNum;
		return this;
	}
}