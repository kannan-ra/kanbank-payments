package com.kanbank.banker.service;

import com.kanbank.banker.dto.AccountDto;
import com.kanbank.banker.entity.Account;
import com.kanbank.banker.entity.AccountBuilder;
import com.kanbank.banker.repository.AccountRepository;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.runners.MockitoJUnitRunner;

import javax.persistence.EntityManager;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class AccountServiceTest {

	@Mock
	private AccountRepository accountRepository;

	@Mock
	private EntityManager em;

	@InjectMocks
	private AccountServiceImpl accountService;

	@Before
	public void setup() {
		MockitoAnnotations.initMocks(this);
	}

	@Test
	public void whenSaveOrUpdateIsCalled_thenAccountIsCreated(){
		// given
		AccountDto accountDto = AccountDto.createFullyPopulated("KanClient", 123456, 12345678);
		Account builtAccount = new AccountBuilder()
				.setFullName(accountDto.getFullName())
				.setSortCode(accountDto.getSortCode())
				.setAccountNum(accountDto.getAccountNum())
				.setId(2L)
				.build();
		when(accountRepository.save(any(Account.class))).thenReturn(builtAccount);

		// when
		accountService.saveOrUpdateAccount(accountDto);

		// then
		ArgumentCaptor<Account> argument = ArgumentCaptor.forClass(Account.class);
		verify(accountRepository).save(argument.capture());
		assertThat(builtAccount, is(argument.getValue()));
	}
}
