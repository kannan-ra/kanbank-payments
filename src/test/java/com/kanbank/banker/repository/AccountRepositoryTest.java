package com.kanbank.banker.repository;

import com.kanbank.banker.configuration.JpaConfig;
import com.kanbank.banker.configuration.TestAppConfig;
import com.kanbank.banker.entity.Account;
import com.kanbank.banker.entity.AccountBuilder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import javax.transaction.Transactional;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.junit.Assert.assertThat;

@RunWith(SpringJUnit4ClassRunner.class)
@EnableTransactionManagement
@ContextConfiguration(classes = {TestAppConfig.class, JpaConfig.class})
public class AccountRepositoryTest {

	@Autowired
	private AccountRepository accountRepository;

	@Test
	@Transactional
	public void testFindBySortCodeAndAccountNum() {
		Account account = new AccountBuilder()
				.setId(2L)
				.setFullName("kan")
				.setSortCode(123456)
				.setAccountNum(12345678)
				.build();
		accountRepository.saveAndFlush(account);
		Account accountRetrieved = accountRepository.findBySortCodeAndAccount(123456, 12345678);
		assertThat(accountRetrieved, is(notNullValue()));
	}
}
