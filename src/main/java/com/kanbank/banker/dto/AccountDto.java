package com.kanbank.banker.dto;

import com.kanbank.banker.entity.Account;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;

@ApiModel (value = "Account")
public class AccountDto implements Serializable {

	private static final long serialVersionUID = -5738654245164646266L;

	@ApiModelProperty(position = 1, required = true, dataType = "String", notes = "Full Name")
	private String fullName;

	@ApiModelProperty(position = 2, required = true, dataType = "int", notes = "Sort Code")
	private int sortCode;

	@ApiModelProperty(position = 3, required = true, dataType = "int", notes = "Account Number")
	private int accountNum;

	/**
	 * Stop construction the usual way
	 */
	private AccountDto() {}

	/**
	 * create a fully populated DTO with all fields specified
	 * @param fullName
	 * @param sortCode
	 * @param accountNum
	 * @return the DTO
	 */
	public static AccountDto createFullyPopulated(String fullName, int sortCode, int accountNum) {
		AccountDto accountDto = new AccountDto();
		accountDto.fullName = fullName;
		accountDto.sortCode = sortCode;
		accountDto.accountNum = accountNum;
		return accountDto;
	}

	/**
	 * create a DTO from its associated Domain Entity
	 * @param account the domain entity
	 * @return the DTO
	 */
	public static AccountDto createFromEntity(Account account) {
		AccountDto accountDto = new AccountDto();
		accountDto.fullName = account.getFullName();
		accountDto.sortCode = account.getSortCode();
		accountDto.accountNum = account.getAccountNum();
		return accountDto;
	}

	/**
	 * The internal auto-generated id is replaced by String combination of SortCode-AccountNum
	 * This masked id used in the URI
	 * @return
	 */
	@ApiModelProperty(hidden = true)
	public String getId() {
		return new StringBuilder()
				.append(sortCode)
				.append('-')
				.append(accountNum)
				.toString();
	}

	public String getFullName() {
		return fullName;
	}

	public int getSortCode() {
		return sortCode;
	}

	public int getAccountNum() {
		return accountNum;
	}
}