package com.kanbank.banker.service;

import com.kanbank.banker.entity.Account;
import com.kanbank.banker.dto.AccountDto;
import com.kanbank.banker.exception.InvalidAccountException;
import com.kanbank.banker.repository.AccountRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer;

@Service("accountService")
@Transactional
class AccountServiceImpl implements AccountService {

	private static final String HYPHEN_STRING = "-";
	
	@Autowired
	private AccountRepository accountRepository;

	public List<AccountDto> findAllAccounts() {
		List<AccountDto> accounts = new ArrayList<AccountDto>();
		for (Account element : accountRepository.findAll()) {
			AccountDto accountDto = AccountDto.createFromEntity(element);
			accounts.add(accountDto);
		}
		return accounts;
	}
	
	public AccountDto findOne(String accId) {
		int sortCode, accountNum;
		try {
			StringTokenizer st = new StringTokenizer(accId, HYPHEN_STRING, false);
			sortCode = Integer.parseInt(st.nextToken());
			accountNum = Integer.parseInt(st.nextToken());
		}
		catch (RuntimeException ex) { // either NumberFormatException or NoSuchElementException
			throw new InvalidAccountException("Invalid account id format");
		}
		Account retrievedEntity = accountRepository.findBySortCodeAndAccount(sortCode, accountNum);
		if (retrievedEntity != null)
			return AccountDto.createFromEntity(retrievedEntity);
		else
			return null;
	}

	public boolean isAccountExist(AccountDto account) {
		return (accountRepository.findBySortCodeAndAccount(account.getSortCode(), account.getAccountNum()) != null);
	}

	public AccountDto saveOrUpdateAccount(AccountDto account) {
		Account entity = new Account(account.getFullName(), account.getSortCode(), account.getAccountNum());
		return AccountDto.createFromEntity(accountRepository.save(entity));
	}

	public void deleteAccount(AccountDto account) {
		Account entity = accountRepository.findBySortCodeAndAccount(account.getSortCode(), account.getAccountNum());
		if (account != null)
			accountRepository.delete(entity);
	}
}
