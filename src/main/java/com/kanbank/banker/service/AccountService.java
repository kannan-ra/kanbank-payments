package com.kanbank.banker.service;

import com.kanbank.banker.dto.AccountDto;

import java.util.List;

public interface AccountService {

	List<AccountDto> findAllAccounts();

	AccountDto findOne(String id);

	boolean isAccountExist(AccountDto account);

	AccountDto saveOrUpdateAccount(AccountDto account);
	
	void deleteAccount(AccountDto account);
}
