package com.kanbank.banker.repository;

import com.kanbank.banker.entity.Account;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface AccountRepository extends JpaRepository<Account, Long> {
	@Query(value = "SELECT a FROM Account a WHERE a.sortCode=:sCode AND a.accountNum=:account")
	Account findBySortCodeAndAccount(@Param("sCode") int sCode, @Param("account") int account);
}
