package com.kanbank.banker.controller;

import com.kanbank.banker.dto.AccountDto;
import com.kanbank.banker.service.AccountService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponentsBuilder;

import java.util.List;

@Api(value = "Account Controller", description = "Account Management APIs")
@RestController
class AccountController {

	@Autowired
	private AccountService accountService;

	@ApiOperation(value = "Get all accounts")
	@RequestMapping(value = "/accounts", method = RequestMethod.GET)
	public ResponseEntity<List<AccountDto>> listAllAccounts() {
		List<AccountDto> accounts = accountService.findAllAccounts();
		if(accounts.isEmpty()){
			return new ResponseEntity<List<AccountDto>>(HttpStatus.NO_CONTENT);//You many decide to return HttpStatus.NOT_FOUND
		}
		return new ResponseEntity<List<AccountDto>>(accounts, HttpStatus.OK);
	}

	@ApiOperation(value = "Get by account id")
	@RequestMapping(value = "/accounts/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<AccountDto> getAccount(@PathVariable("id") String accId) {
		AccountDto account = accountService.findOne(accId);
		if (account == null) {
			return new ResponseEntity<AccountDto>(HttpStatus.NOT_FOUND);
		}
		return new ResponseEntity<AccountDto>(account, HttpStatus.OK);
	}

	@ApiOperation(value = "Create new account")
	@RequestMapping(value = "/accounts", method = RequestMethod.POST)
	public ResponseEntity<Void> createAccount(@RequestBody AccountDto account, UriComponentsBuilder ucBuilder) {
		if (accountService.isAccountExist(account)) {
			return new ResponseEntity<Void>(HttpStatus.CONFLICT);
		}
		AccountDto savedAccount = accountService.saveOrUpdateAccount(account);
		HttpHeaders headers = new HttpHeaders();
		headers.setLocation(ucBuilder.path("/accounts/{uri}").buildAndExpand(savedAccount.getId()).toUri());
		return new ResponseEntity<Void>(headers, HttpStatus.CREATED);
	}

	@ApiOperation(value = "Delete account")
	@RequestMapping(value = "/accounts/{id}", method = RequestMethod.DELETE)
	public ResponseEntity<AccountDto> deleteAccount(@PathVariable("id") String accId) {
		AccountDto account = accountService.findOne(accId);
		if (account == null) {
			return new ResponseEntity<AccountDto>(HttpStatus.NOT_FOUND);
		}
		accountService.deleteAccount(account);
		return new ResponseEntity<AccountDto>(HttpStatus.NO_CONTENT);
	}
}