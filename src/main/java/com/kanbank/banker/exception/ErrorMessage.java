package com.kanbank.banker.exception;

import java.io.Serializable;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class ErrorMessage implements Serializable {

	private static final long serialVersionUID = -8236249944690855297L;

	private List<String> errors;

	public ErrorMessage() {
	}

	public ErrorMessage(List<String> errors) {
		this.errors = errors;
	}

	public ErrorMessage(String error) {
		this(Collections.singletonList(error));
	}

	public ErrorMessage(String ... errors) {
		this(Arrays.asList(errors));
	}

	public List<String> getErrors() {
		return errors;
	}

	public void setErrors(List<String> errors) {
		this.errors = errors;
	}
}