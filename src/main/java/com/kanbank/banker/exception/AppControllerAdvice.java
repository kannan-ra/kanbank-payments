package com.kanbank.banker.exception;

import org.springframework.dao.DataAccessException;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import javax.servlet.http.HttpServletRequest;

@ControllerAdvice(basePackages = {"com.kanbank.banker.controller"} )
public class AppControllerAdvice {
	@ExceptionHandler(InvalidAccountException.class)
	@ResponseStatus(HttpStatus.BAD_REQUEST)
	@ResponseBody
	ErrorMessage handleInvalidAccountException(HttpServletRequest req, InvalidAccountException ex) {
		ex.printStackTrace();
		ErrorMessage errorMessage = new ErrorMessage(ex.getMessage());
		return errorMessage;
	}

	@ExceptionHandler(DataAccessException.class)
	@ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
	@ResponseBody
	ErrorMessage handleDatabaseException(HttpServletRequest req, DataAccessException ex) {
		ex.printStackTrace();
		ErrorMessage errorMessage = new ErrorMessage(ex.getMessage());
		return errorMessage;
	}

	@ExceptionHandler(RuntimeException.class)
	@ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
	@ResponseBody
	ErrorMessage handleRuntimeException(HttpServletRequest req, RuntimeException ex) {
		ex.printStackTrace();
		ErrorMessage errorMessage = new ErrorMessage(ex.getMessage());
		return errorMessage;
	}
}
