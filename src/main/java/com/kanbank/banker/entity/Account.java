package com.kanbank.banker.entity;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="account_details")
public class Account {

	@Id
	@Column(name = "id")
	@GeneratedValue
	private Long id;

	@Column(name = "full_name", unique = false, nullable = false)
	private String fullName;

	@Column(name = "sort_code", nullable = false)
	private int sortCode;

	@Column(name = "account_num", nullable = false)
	private int accountNum;

	public Account() { id = -1L; }

	public Account(String fullName, int sortCode, int accountNum) {
		this.fullName = fullName;
		this.sortCode = sortCode;
		this.accountNum = accountNum;
	}

	public String getFullName() {
		return fullName;
	}

	public int getSortCode() {
		return sortCode;
	}

	public int getAccountNum() {
		return accountNum;
	}

	public Long getId() {
		return id;
	}

	public void setFullName(String fullName) {
		this.fullName = fullName;
	}

	public void setSortCode(int sortCode) {
		this.sortCode = sortCode;
	}

	public void setAccountNum(int accountNum) {
		this.accountNum = accountNum;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;

		if (o == null || getClass() != o.getClass()) return false;

		Account that = (Account) o;

		return new EqualsBuilder()
				.append(sortCode, that.sortCode)
				.append(accountNum, that.accountNum)
				.append(fullName, that.fullName)
				.isEquals();
	}

	@Override
	public int hashCode() {
		return new HashCodeBuilder(17, 37)
				.append(fullName)
				.append(sortCode)
				.append(accountNum)
				.toHashCode();
	}

	@Override
	public String toString() {
		return "Account{" +
				"id=" + id +
				", fullName='" + fullName + '\'' +
				", sortCode=" + sortCode +
				", accountNum=" + accountNum +
				'}';
	}
}