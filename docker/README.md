### Docker files in the project
* `Dockerfile` - specific to each container
* `docker-compose.yml` - to run multi-container setup

Ref:
* [Docker MySQL](https://hub.docker.com/_/mysql/)
* [Docker Tomcat](https://hub.docker.com/_/tomcat/)


### Docker Commands
`docker --help` will produce comprehensive man page

```
# list the containers including stopped ones
docker ps -a

# build a container based on the current directory's 'Dockerfile'
docker build -t <image-tag> .

# run a container as daemon, port-mapping, volume mounted with a name
docker run -it -d -p 8080:8080 -v <local/dir>:<mounted/on/contr/dir> --name <contr-name> <contr-image>

# stop a running container
docker stop <contr>

# remove a stopped container
docker rm <contr>

# list the docker images
docker images

# romove a image - with no container dependent on it
docker rmi <image>

# connect/login to container
docker exec -it <contr> /bin/bash
(or)
docker attach <contr>

# to view logs in a container
docker logs <contr>
```

eg. 
`docker build -t itomcat .; docker stop ctomcat; docker rm ctomcat; docker run -t -p 8080:8080 --name ctomcat itomcat`


### Docker Compose commands
Docker Compose ease up the process of container build & deployments. So we could still use docker commands on the containers spun by compose. 
```
# build a containers as specified by 'docker-compose.yml' file - assume its in curr directory
docker-compose build

# build and run the containers in one go
docker-compose up --build

# bring up all the containers & run as daemon
docker-compose up -d

# shutdown the containers started by docker-compose.yml
docker-compose down
```

### Mounting volumes
* We can mount single file or a directory
 * Tomcat has `./target/banker.war:/usr/local/tomcat/webapps/banker.war` mounted as single file
 * MySql has `./docker/mysql/data:/var/lib/mysql` mounted directory
* `.war` mount helps to continuously deploy newly built project without docker restart


### Notes
* If the port bind fails on std 3306, stop Mysql Server service if running locally
* Same for tomcat
* If MySql is not getting connected to tomcat
 * Just specify the links in the docker compose & they would be able to connect
 * also change the JDBC Url in the code to match the container name
* Specifying the data volume on the host for the mysql to retain data across container lifecycle
 * Currently I am persisting the data locally at `./docker/mysql/data` & mounted at `/var/lib/mysql`

##### KNOWN ISSUES
* During the first provisioning time, mysql database is written at `./docker/mysql/data` & is slow - hence tomcat can't connect to MySQL. So need to run `docker-compose up` again
* When you `mvn clean install` the container volume binding for war file is broken & container will not redeploy; `mvn install` will redeploy