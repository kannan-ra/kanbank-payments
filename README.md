# Kanbank Payments

_I am doing this project out of motivation to learn in building scalable api_

**KANBANK** is a fictional high street retail bank. This project deals with scenario where their retail customers are trying to pay someone.


## Build Instructions

```
cd kanbank
mvn clean install
docker-compose up --build
```


## Swagger Frontend
Once the containers are up and running without error, swagger can help with frontend to test the api at [http://localhost:8080/banker/swagger-ui.html]()



## Workflow
* retail customers initiate payments to someone or some business
* bank receives the payment instructions
* bank debits the money from `from` account & credit its own `internal holding` account
* bank creates batch payment from its holding account to all original payees
* these batched payments are sent to another service called `Central Processing Service`
* CPS then translates the payments into BACS format, which will be cleared in `VOCALINK`

## Challenges
* High Volume Transaction processing
* Consistency & Availability
* Cloud Scalable

## Technology Stack
* Java, Spring, Hibernate, Junit, Mockito
* MySQL (may be Mongodb later on)
* RabbitMQ, Apache Camel
* Hadoop
* Logstash
* Docker, Chef
* Jenkins
* Git
* Amazon AWS

